﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIStars : MonoBehaviour 
{
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private int levelId;

    [SerializeField]
    private Image[] starImages;
    [SerializeField]
    private Sprite activeStarSprite;
    [SerializeField]
    private Sprite noActiveStarSprite;

    void OnEnable()
    {
        if (levelId - 1 < gameManager.Levels.Length)
        {
            int stars = gameManager.Levels[levelId - 1].Stars;
            for (int i = 0; i < 3; i++)
            {
                if (i + 1 <= stars)
                {
                    starImages[i].sprite = activeStarSprite;
                }
                else 
                {
                    starImages[i].sprite = noActiveStarSprite;
                }
            }
        }
    }
}
