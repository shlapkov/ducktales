﻿using UnityEngine;
using System.Collections;

public class Shadow : MonoBehaviour
{
    public static Vector2 lightDirection;
    public GameObject parent;

    void Start()
    {
        lightDirection.x = .5f;
        lightDirection.y = .5f;
        lightDirection = lightDirection.normalized;
    }

    [SerializeField]
    private float shadow_offset;

    void FixedUpdate()
    {
        Vector3 diff = (parent.transform.position - new Vector3(lightDirection.x, lightDirection.y, 0.0f)).normalized * shadow_offset;
        transform.position = parent.transform.position + diff;
    }
}
