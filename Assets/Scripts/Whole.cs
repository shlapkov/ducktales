﻿using UnityEngine;
using System.Collections;

public class Whole : MonoBehaviour
{
    // Воронка работает с помощью триггер Collider2D 

    [SerializeField]
    private float minForceMagnitude = 2.7f; // если магнитуда вектора скорости утки меньше этого значения, то воронка инкрементирует до этого значения
    // P.S. Это сделано для того, чтобы утка не стала в воронке и не колбасилась в центре, а держалась на определенном от него расстоянии как бы кружась в воронке

    [SerializeField]
    private float incrementForce = 0.05f; // Собственно на это значение каждый фрейм инкрементируется значение магнитуды утки, если она медленнее minForceMagnitude 

    [SerializeField]
    private float angularFactor = 0.3f; // коэффициент притягивания, чем больше, тем сильнее притягивает воронка к центру

    void OnTriggerStay2D(Collider2D other) // НЕ ТРОГАТЬ, ВСЕ РАБОТАЕТ :)
    {
        Vector3 dir = (other.transform.position - transform.position).normalized; // Вектор от воронки к игроку
        Vector3 up = new Vector3(0.0f, 0.0f, -1.0f); // Вектор направленный вертикально вверх в экран

        Vector2 force = new Vector3(other.attachedRigidbody.velocity.x, other.attachedRigidbody.velocity.y, 0.0f) * 2.0f + Vector3.Cross(dir, up) + ((dir * -1.0f) * angularFactor); // Высчитываем вектор направления скорости

        float forceMagnitude = other.attachedRigidbody.velocity.magnitude;

        if (forceMagnitude < minForceMagnitude)
        {
            forceMagnitude += incrementForce;
        }

        other.attachedRigidbody.velocity = (other.attachedRigidbody.velocity + force).normalized * forceMagnitude;
    }
}
