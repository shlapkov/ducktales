﻿using UnityEngine;
using System.Collections;

public class DynamicBarrier : MonoBehaviour
{
    [SerializeField]
    private float minDistance = 1.0f;
    [SerializeField]
    private float maxDistance = 5.0f;
    [SerializeField]
    private float factor = 500.0f;

    private Rigidbody2D _rdbody;

    void OnEnable()
    {
        if (_rdbody == null)
        {
            _rdbody = GetComponent<Rigidbody2D>();
        }

        Bath.OnTouch += Bath_OnTouch;
    }

    void OnDisable()
    {
        Bath.OnTouch -= Bath_OnTouch;
    }

    void Bath_OnTouch (Vector3 position)
    {
        Vector3 dir = (transform.position - position).normalized;

        float distance = Helpers.DistanceXY(transform.position, position);

        if ((distance > maxDistance) || (distance <= minDistance))
        {
            return;
        }

        float forceFactor = factor / (distance / maxDistance); 
        _rdbody.AddForceAtPosition(dir * forceFactor, position);
    }
}
