﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private float minDistance = 1.0f;
    [SerializeField]
    private float maxDistance = 5.0f;
    [SerializeField]
    private float factor = 500.0f;

    private int countStar;
    private Rigidbody2D _rdbody;

    public int CountStar { get { return countStar; } }

    void OnEnable()
    {
        if (_rdbody == null)
        {
            _rdbody = GetComponent<Rigidbody2D>();
        }

        if (gameManager == null)
        {
            gameManager = FindObjectOfType<GameManager>();
        }

        Bath.OnTouch += Bath_OnTouch;
    }

    void OnDisable()
    {
        Bath.OnTouch -= Bath_OnTouch;
    }

    public void Restart()
    {
        _rdbody.position = new Vector2(-5.32f, -0.55f);
        _rdbody.velocity = Vector2.zero;
        _rdbody.angularVelocity = 0;
        _rdbody.rotation = 0;

        countStar = 0;
    }

    void Bath_OnTouch (Vector3 position)
    {
        Vector3 dir = (transform.position - position).normalized;

        float distance = Helpers.DistanceXY(transform.position, position);

        if ((distance > maxDistance) || (distance <= minDistance))
        {
            return;
        }

        float forceFactor = factor / (distance / maxDistance); 

        _rdbody.AddForceAtPosition(dir * forceFactor, position);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Finish"))
        {
            gameManager.OnVictory(countStar);
        }
        else if (other.gameObject.tag.Equals("Star"))
        {
            countStar++;
            other.transform.parent.gameObject.SetActive(false);
        }
    }

}
