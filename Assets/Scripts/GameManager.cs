﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private const string STAR = "STAR";

    [SerializeField]
    private LevelOptions[] levels;
    private int currLevelId = 0;
    private float currTimer;
    private bool hasTimer = true;
    private GameObject currentScene;

    private GameState state;

    public LevelOptions[] Levels { get { return levels; } }

    void Awake()
    {
        LoadFromPlayerPrefs();
    }

    void Start() // Заглушка
    {
        currTimer = levels[currLevelId].Timer;
    }

    public void ChangeGameState(GameState nextState)
    {
        state = nextState;

        if (OnChangeGameState != null)
        {
            OnChangeGameState(state);
        }
    }

    public void InitLevel(int levelId)
    {
        if (levelId - 1 < levels.Length)
        {
            ChangeGameState(GameState.Game);
            currTimer = levels[currLevelId].Timer;
            currLevelId = levelId - 1;

            if (currTimer < 0.0f)
            {
                hasTimer = false;
            }
            else
            {
                hasTimer = true;
            }

            // Load prefab
            LoadCurrentScene();
        }
        else
        {
            Debug.LogWarning("Not find level prefab");
        }
    }

    public void RestartLevel()
    {
        ChangeGameState(GameState.Game);
        currTimer = levels[currLevelId].Timer;
        LoadCurrentScene();
    }

    void FixedUpdate()
    {
        if (state == GameState.Game && hasTimer)
        {
            currTimer -= Time.fixedDeltaTime;

            GameObject.FindGameObjectWithTag("Timer").GetComponent<Text>().text = currTimer.ToString("F1");

            if (currTimer <= 0.0f)
            {
                OnDefeat();
            }
        }
    }

    public void OnDefeat()
    {
        ChangeGameState(GameState.GameOver);
    }

    public void OnVictory(int currPlayerStar)
    {
        if (levels[currLevelId].Stars < currPlayerStar)
        {
            levels[currLevelId].Stars = currPlayerStar;

            SaveToPlayerPrefs();
        }

        ChangeGameState(GameState.Finish);
    }

    public void OnExit()
    {
        // @TODO
    }

    private void SaveToPlayerPrefs()
    {
        //save stars
        for (int i = 0; i < levels.Length; i++)
        {
            PlayerPrefs.SetInt(STAR + "_" + i.ToString(), levels[i].Stars);
        }

        PlayerPrefs.Save();
    }

    private void LoadFromPlayerPrefs()
    {
        for (int i = 0; i < levels.Length; i++)
        {
            levels[i].Stars = PlayerPrefs.GetInt(STAR + "_" + i.ToString(), 0);
        }
    }

    public delegate void GameStateDelegate(GameState state);
    public static event GameStateDelegate OnChangeGameState;

    // Helper
    private void LoadCurrentScene()
    {
        Destroy(currentScene);
        currentScene = Instantiate(levels[currLevelId].scene, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity) as GameObject;    
    }
}

public enum GameState 
{
    Main, SelectLevel, Game, GameOver, Finish, Credits
}