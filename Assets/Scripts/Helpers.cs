﻿using UnityEngine;
using System.Collections;

public class Helpers : MonoBehaviour
{
    public static float DistanceXY(Vector3 a, Vector3 b)
    {
        float xD = a.x - b.x;
        float yD = a.y - b.y;
        float dist2 = xD * xD + yD * yD;
        return dist2;
    }
}
