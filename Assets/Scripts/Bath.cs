﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Bath : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    public GameObject[] touchAnimations;

    void PlayWaveAnimation(GameObject anim, Vector3 position)
    {
        // REWORK
        anim.transform.position = new Vector3(position.x, position.y, 0.0f);
        anim.SetActive(true);
        //GameObject go = Instantiate(anim, position, Quaternion.identity) as GameObject;
        //anim.GetComponent<Animator>().Play();
    }

    private GameObject GetNotActiveAnimation()
    {
        for (int i = 0; i < touchAnimations.Length; i++)
        {
            if (!touchAnimations[i].activeSelf)
            {
                return touchAnimations[i];
            }
        }

        return null;
    }

    #region IPointerClickHandler implementation
    public void OnPointerClick(PointerEventData eventData)
    {
        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -10.0f));

        if (OnTouch != null)
        {
            OnTouch(position);

            GameObject notActiveAnim = GetNotActiveAnimation();
            if (notActiveAnim != null)
            {
                PlayWaveAnimation(notActiveAnim, position);
            }
            else
            {
                Debug.LogWarning("Did not find not active animation");
            }
        }
    }
    #endregion

    public delegate void OnTouchDelegate(Vector3 position);
    public static event OnTouchDelegate OnTouch;

}
