﻿using UnityEngine;
using System.Collections;

public class LevelOptions : MonoBehaviour
{
    public GameObject scene;

    [SerializeField]
    private float timer;

    [SerializeField]
    int stars;

    public float Timer
    {
        get
        {
            return timer;
        }
    }

    public int Stars
    {
        get
        {
            return stars;
        }
        set 
        { 
            stars = value; 
        }
    }
}
