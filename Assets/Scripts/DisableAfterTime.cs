﻿using UnityEngine;
using System.Collections;

public class DisableAfterTime : MonoBehaviour 
{
    [SerializeField]
    private float time = 1.0f;

    void OnEnable()
    {
        StartCoroutine(Action());
    }

    private IEnumerator Action()
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }
}
