﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelectSlider : MonoBehaviour
{
    [SerializeField]
    private float sensitivity = 10.0f;
    private Image bg;

    void Start()
    {
        bg = GetComponent<Image>();
    }

    void FixedUpdate()
    {
        Vector3 delta = (Vector2.right * Input.mouseScrollDelta.y * sensitivity);
        bg.rectTransform.position += delta;

        if(bg.rectTransform.position.x < -2.0f)
        {
            bg.rectTransform.position = new Vector3(-2.0f, bg.rectTransform.position.y, 0.0f);
        }

        if (bg.rectTransform.position.x > 580.0f)
        {
            bg.rectTransform.position = new Vector3(580.0f, bg.rectTransform.position.y, 0.0f);
        }
    }
}
