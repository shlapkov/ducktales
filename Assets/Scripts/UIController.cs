﻿// UIController.cs
// Author:  Andrey Shlapkov <a1@indigobunting.net>
// Copyright (c) 2017 Indigo Bunting
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;  

    [Header("Panels")]
    [SerializeField]
    private GameObject menu;
    [SerializeField]
    private GameObject selectLevel;
    [SerializeField]
    private GameObject game;
    [SerializeField]
    private GameObject finish;
    [SerializeField]
    private GameObject gameOver;
    [SerializeField]
    private GameObject credits;

    void OnEnable()
    {
        GameManager.OnChangeGameState += GameManager_OnChangeGameState;
    }

    void OnDisable()
    {
        GameManager.OnChangeGameState -= GameManager_OnChangeGameState;
    }

    private void GameManager_OnChangeGameState(GameState state)
    {
        switch (state)
        {
            case GameState.Main:
                menu.gameObject.SetActive(true);
                selectLevel.gameObject.SetActive(false);
                game.gameObject.SetActive(false);
                finish.gameObject.SetActive(false);
                gameOver.gameObject.SetActive(false);
                credits.gameObject.SetActive(false);
                break;
            case GameState.SelectLevel:
                menu.gameObject.SetActive(false);
                selectLevel.gameObject.SetActive(true);
                game.gameObject.SetActive(false);
                finish.gameObject.SetActive(false);
                gameOver.gameObject.SetActive(false);
                credits.gameObject.SetActive(false);
                break;
            case GameState.Game:
                menu.gameObject.SetActive(false);
                selectLevel.gameObject.SetActive(false);
                game.gameObject.SetActive(true);
                finish.gameObject.SetActive(false);
                gameOver.gameObject.SetActive(false);
                credits.gameObject.SetActive(false);
                break;
            case GameState.Finish:
                menu.gameObject.SetActive(false);
                selectLevel.gameObject.SetActive(false);
                game.gameObject.SetActive(false);
                finish.gameObject.SetActive(true);
                gameOver.gameObject.SetActive(false);
                credits.gameObject.SetActive(false);
                break;
            case GameState.GameOver:
                menu.gameObject.SetActive(false);
                selectLevel.gameObject.SetActive(false);
                game.gameObject.SetActive(false);
                finish.gameObject.SetActive(false);
                gameOver.gameObject.SetActive(true);
                credits.gameObject.SetActive(false);
                break;
            case GameState.Credits:
                menu.gameObject.SetActive(false);
                selectLevel.gameObject.SetActive(false);
                game.gameObject.SetActive(false);
                finish.gameObject.SetActive(false);
                gameOver.gameObject.SetActive(false);
                credits.gameObject.SetActive(true);
                break;


        }
    }

    public void ClickToSelectLevel()
    {
        gameManager.ChangeGameState(GameState.SelectLevel);
    }

    public void ClickPlay(int levelId)
    {
        gameManager.InitLevel(levelId);
    }

    public void ClickToMainMenu()
    {
        gameManager.ChangeGameState(GameState.Main);
    }

    public void ClickToCredits()
    {
        gameManager.ChangeGameState(GameState.Credits);
    }

}


